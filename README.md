This dataset is a annotated pothole dataset. The dataset is written in `Yolo Darknet .txt` format. 
The .txt file consists of `class, x_center, y_center, width, and height` for each row. All the informations 
are `normalised between 0-1 except for the class`. To convert the normalized `x_center` and `y_center` into 
a pixels format, simply multiply it with the width(x_center) or height(y_center) of the image. Same goes 
to the normalized width and height. The `class` numbers are `zero-indexed` (start from 0).There are 
`1564 images` with `720x480` in size. There are `3530 annotated potholes` inside this dataset.